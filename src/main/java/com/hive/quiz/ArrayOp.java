package com.hive.quiz;

/**
 * Created by valenciap on 03/09/2015.
 */
public class ArrayOp {

    public ArrayOp() {
        super();
    }

    /**
     * Returns the first index of the subarray if matches. Otherwise return -1.
     * @param mainArray
     * @param subArray
     * @return
     */
    public int firstIndexOfSubarray(int[] mainArray, int[] subArray) {

        //Null cases
        if(isNullOrEmpty(mainArray) ||
                isNullOrEmpty(subArray) ||
                isLongerThan(subArray,mainArray)) {
            return -1;
        }

        int subArrayStartsAt = -1;

        int indexMainArray = 0;
        int indexSubArray = 0;
        int subArrayLength = subArray.length;
        int mainArrayLength = mainArray.length;

        while((indexMainArray < mainArrayLength) && (indexSubArray < subArrayLength)) {
            if(mainArray[indexMainArray] == subArray[indexSubArray]) {
                //if mainArray and subArray match in position, we need to check if already updated
                if(subArrayStartsAt == -1) {
                    subArrayStartsAt = indexMainArray;
                }
                indexSubArray++;
            } else {
                subArrayStartsAt = -1;
            }
            indexMainArray++;
        }

        //We need to check if the subarray finished
        if(subArrayLength != indexSubArray) {
           subArrayStartsAt  = -1;
        }

        return subArrayStartsAt;
    }


    private boolean isNullOrEmpty(int[] array) {
        return (array == null || array.length == 0);
    }

    private boolean isLongerThan(int[] array1, int[] array2) {
        return array1.length > array2.length;
    }
}
