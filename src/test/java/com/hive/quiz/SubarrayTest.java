package com.hive.quiz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by valenciap on 03/09/2015.
 */
public class SubarrayTest {

    private ArrayOp arrayOp;

    @Before
    public void setUp() throws Exception{
        arrayOp = new ArrayOp();
    }

    @Test
    public void mainArrayNull() throws Exception {
        int[] subArray = {4,5};

        int result = arrayOp.firstIndexOfSubarray(null, subArray);
        Assert.assertEquals("Error getting index of subarray", result, -1);
    }

    @Test
    public void subArrayNull() throws Exception {
        int[] mainArray = {1,2,3,4,5,6};

        int result = arrayOp.firstIndexOfSubarray(mainArray, null);
        Assert.assertEquals("Error getting index of subarray", result, -1);

    }

    @Test
    public void mainArrayEmpty() throws Exception {
        int[] mainArray = {};
        int[] subArray = {4,5};

        int result = arrayOp.firstIndexOfSubarray(mainArray, subArray);
        Assert.assertEquals("Error getting index of subarray", result, -1);
    }

    @Test
    public void subArrayEmpty() throws Exception {
        int[] mainArray = {1,2,3,4,5,6};
        int[] subArray = {};

        int result = arrayOp.firstIndexOfSubarray(mainArray, subArray);
        Assert.assertEquals("Error getting index of subarray", result, -1);

    }

    @Test
    public void subArrayGreaterThanMainArray() throws Exception {
        int[] mainArray = {1,2,3};
        int[] subArray = {2,3,4,5};

        int result = arrayOp.firstIndexOfSubarray(mainArray, subArray);
        Assert.assertEquals("Error getting index of subarray", result, -1);
    }

    @Test
    public void subArrayContainedInMainArray() throws Exception {
        int[] mainArray = {2,3,4,5};
        int[] subArray = {3,4};

        int result = arrayOp.firstIndexOfSubarray(mainArray, subArray);
        Assert.assertEquals("Error getting index of subarray", result, 1);
    }

    @Test
    public void subArrayNotContainedInMainArray() throws Exception {
        int[] mainArray = {2,3,4,5};
        int[] subArray = {4,5,6};

        int result = arrayOp.firstIndexOfSubarray(mainArray, subArray);
        Assert.assertEquals("Error getting index of subarray", result, -1);
    }

    @Test
    public void subArrayEqualsThanMainArray() throws Exception {
        int[] mainArray = {2,3,4,5};
        int[] subArray = {2,3,4,5};

        int result = arrayOp.firstIndexOfSubarray(mainArray, subArray);
        Assert.assertEquals("Error getting index of subarray", result, 0);
    }
}
